
apply_migrations:
	python -c 'from app.models import apply_migrations; apply_migrations()'


rollback_migrations:
	python -c 'from app.models import apply_migrations; rollback_migrations()'
