import asyncio
import typing as t

from starlette.requests import Request

import uvicorn
from fastapi import Depends, FastAPI, HTTPException, File, UploadFile
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from pydantic import BaseModel, Field
from tortoise.contrib.fastapi import register_tortoise
from tortoise.exceptions import IntegrityError

from . import config
from . import models

app = FastAPI()

register_tortoise(app, db_url=config.DB_URL, modules={'models': ['app.models']}, add_exception_handlers=True)


class APIException(HTTPException):
    status_code = NotImplemented
    detail = ''

    def __init__(self, headers: dict = None):
        super().__init__(self.status_code, self.detail, headers)


class IncorrectAuthData(APIException):
    status_code = 400
    detail='Incorrect username or password'


class InvalidAuthCredentials(APIException):
    status_code = 401
    detail = 'Invalid authentication credentials'


class UserAlreadyExists(APIException):
    status_code = 400
    detail = 'User with provided username already exists'


class UserProfileNotFound(APIException):
    status_code = 404
    detail = 'User profile not found'


class WrongFileContentType(APIException):
    status_code = 400
    detail = 'Wrong content type of file'


class BaseUser(BaseModel):
    username: str
    interests: t.Optional[str]

    class Config:
        orm_mode = True


class UserCreate(BaseUser):
    password: str


class UserUpdate(BaseModel):
    password: t.Optional[str]
    interests: t.Optional[str]

    class Config:
        orm_mode = True


class UserProfile(BaseModel):
    id: int
    username: str
    interests: t.Optional[str]
    avatar_url: t.Optional[str]
    score: int = 0

    class Config:
        orm_mode = True


class Score(BaseModel):
    amount: int = Field(..., gt=0)


@app.post('/users/register/', response_model=UserProfile)
async def register_user(user_data: UserCreate):
    user = models.User(**user_data.dict())
    try:
        await user.save(update_fields=user_data.fields.keys())
    except IntegrityError:
        raise UserAlreadyExists

    return UserProfile.from_orm(user)


@app.post('/token/')
async def login(auth_data: OAuth2PasswordRequestForm = Depends()):
    user = await models.User.get_or_none(username=auth_data.username).prefetch_related('token')

    if not user:
        raise IncorrectAuthData

    if not user.is_correct_password(auth_data.password):
        raise IncorrectAuthData

    token, _ = await models.Token.get_or_create(user=user)
    return {'access_token': token.token, "token_type": "bearer"}


oauth_schema = OAuth2PasswordBearer(tokenUrl='/token/')


async def current_user(token: str = Depends(oauth_schema)) -> models.User:
    user = await models.User.get_or_none(token__token=token).prefetch_related('token')
    if not user:
        raise InvalidAuthCredentials

    return user


@app.get('/users/profile/me/', response_model=UserProfile)
async def get_current_profile(user: models.User = Depends(current_user)):
    return UserProfile.from_orm(user)


@app.get('/users/profiles/', response_model=t.Iterable[UserProfile])
async def user_profiles_list(count: int = 30, page: int = 0):
    users_qs = await models.User.all().order_by('-created_at').limit(count).offset(count * page)

    return (UserProfile.from_orm(user) for user in users_qs)


@app.get('/users/{user_id}/profile/', response_model=UserProfile)
async def get_user_profile(user_id: int):
    user = await models.User.get_or_none(id=user_id)
    if not user:
        raise UserProfileNotFound
    
    return UserProfile.from_orm(user)


@app.put('/users/profile/', response_model=UserProfile)
async def update_user_profile(
    profile_data: UserUpdate,
    user: models.User = Depends(current_user)
):
    update_data = profile_data.dict(exclude_unset=True)
    user.update_from_dict(update_data)
    await user.save(update_fields=update_data.keys())

    return UserProfile.from_orm(user)


@app.delete('/users/profile/')
async def delete_user(user: models.User = Depends(current_user)):
    await user.delete()


@app.post('/users/score/up/')
async def up_user_score(score_data: Score, user: models.User = Depends(current_user)):
    user.score += score_data.amount
    await user.save(update_fields=['score'])

    return {'score': user.score}


@app.post('/users/score/down/')
async def down_user_score(score_data: Score, user: models.User = Depends(current_user)):
    user.score -= score_data.amount
    await user.save(update_fields=['score'])

    return {'score': user.score}


# def create_file(file_data: bytes = File(...)):

from . import storage


from io import BytesIO



@app.post('/users/profile/avatar/')
async def upload_avatar(
    request: Request,
    file: UploadFile = File(...),
    user: models.User = Depends(current_user),
):
    if 'image' not in file.content_type:
        raise WrongFileContentType

    avatar = file.file
    length = len(avatar.read())
    avatar.seek(0)

    avatar_url = storage.put_object(avatar, content_type=file.content_type, length=length)
    user.avatar_url = avatar_url
    await user.save(update_fields=['avatar_url'])

    return {'avatar_url': avatar_url}


@app.delete('/users/profile/avatar/')
async def delete_avatar(user: models.User = Depends(current_user)):
    user.avatar_url = None
    await user.save(update_fields=['avatar_url'])
