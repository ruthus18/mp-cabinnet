import datetime as dt
import hashlib
import uuid

import yoyo

from tortoise import Tortoise
from tortoise import models
from tortoise import fields
from tortoise.fields.relational import OneToOneField
from tortoise.fields.data import DatetimeField

from . import config


async def init_db():
    await Tortoise.init(db_url=config.DB_URL, modules={'models': ['app.models']})


def apply_migrations():
    backend = yoyo.get_backend(config.DB_URL)
    migrations = yoyo.read_migrations('app/migrations')

    with backend.lock():
        to_apply = backend.to_apply(migrations)
        backend.apply_migrations(to_apply)


def rollback_migrations():
    backend = yoyo.get_backend(config.DB_URL)
    migrations = yoyo.read_migrations('app/migrations')

    with backend.lock():
        to_rollback = backend.to_rollback(migrations)
        backend.rollback_migrations(to_rollback)


def _get_hashed_password(password: str) -> str:
    return hashlib.sha512((password + config.SECRET_KEY).encode('utf-8')).hexdigest()


def _generate_token() -> str:
    return uuid.uuid4().hex 


class Token(models.Model):
    # TODO: протухание токенов

    id = fields.IntField(pk=True)
    user = OneToOneField(model_name='models.User', on_delete='CASCADE', related_name='token')
    token = fields.TextField(unqiue=True, default=_generate_token())

    created_at = fields.DatetimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.user} - {self.token}'


class User(models.Model):
    id = fields.IntField(pk=True)
    username = fields.CharField(max_length=32, unique=True)
    password = fields.CharField(max_length=128)

    interests = fields.TextField(null=True)
    avatar_url = fields.TextField(null=True)
    score = fields.IntField(default=0)

    created_at = fields.DatetimeField(auto_now_add=True)

    def __str__(self):
        return self.login

    def set_password(self, password: str):
        return _get_hashed_password(password)

    def is_correct_password(self, password: str) -> bool:
        return _get_hashed_password(password) == self.password

    async def save(self, *args, **kwargs):
        update_fields = kwargs.get('update_fields')

        if update_fields and 'password' in update_fields:
            self.password = self.set_password(self.password)

        return await super().save(*args, **kwargs)
