import io
import sys
import uuid
import mimetypes

from minio import Minio
from minio.error import BucketAlreadyOwnedByYou, BucketAlreadyExists

from . import config


minio_client = Minio(
    f'{config.MINIO_HOST}:{config.MINIO_PORT}',
    access_key=config.MINIO_ACCESS_KEY,
    secret_key=config.MINIO_SECRET_KEY,
    secure=False if config.DEBUG else True,
)

def init_storage():
    try:
        minio_client.make_bucket(config.MINIO_BUCKET_NAME)
    except (BucketAlreadyExists, BucketAlreadyOwnedByYou):
        pass


def put_object(file_data: io.RawIOBase, length: int, filename: str = None, content_type=None):
    if not filename:
        filename = uuid.uuid4().hex

    file_extension = ''
    if content_type:
        file_extension = mimetypes.guess_extension(content_type)

    minio_client.put_object(
        bucket_name=config.MINIO_BUCKET_NAME,
        object_name=filename + file_extension,
        data=file_data,
        length=length,
        content_type=content_type
    )

    return f'{config.MINIO_HOST}:{config.MINIO_PORT}/{config.MINIO_BUCKET_NAME}/{filename}{file_extension}'
