SECRET_KEY = 'wow!such!secret!key'

DEBUG = True
AUTORELOAD = True

DB_HOST = 'localhost'
DB_PORT = 5432
DB_NAME = 'mp'
DB_USER = 'mp'
DB_PASSWORD = 'mp'

DB_URL = f'postgres://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}'

MINIO_HOST = 'localhost'
MINIO_PORT = 9000
MINIO_ACCESS_KEY = 'mp-access'
MINIO_SECRET_KEY = 'mp-secretkey'
MINIO_BUCKET_NAME = 'storage'
