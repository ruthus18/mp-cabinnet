from yoyo import step


def apply_step(conn):
    cursor = conn.cursor()
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS "user" (
            "id" SERIAL NOT NULL PRIMARY KEY,
            "username" VARCHAR(32) NOT NULL UNIQUE,
            "password" VARCHAR(128) NOT NULL,
            "interests" TEXT,
            "avatar_url" TEXT,
            "score" INT NOT NULL  DEFAULT 0,
            "created_at" TIMESTAMP NOT NULL  DEFAULT CURRENT_TIMESTAMP
        );
        CREATE TABLE IF NOT EXISTS "token" (
            "id" SERIAL NOT NULL PRIMARY KEY,
            "token" TEXT NOT NULL,
            "created_at" TIMESTAMP NOT NULL  DEFAULT CURRENT_TIMESTAMP,
            "user_id" INT NOT NULL UNIQUE REFERENCES "user" ("id") ON DELETE CASCADE
        );
    """)


def rollback_step(conn):
    cursor = conn.cursor()
    cursor.execute("""
        DROP TABLE IF EXISTS "token";
        DROP TABLE IF EXISTS "user";
    """)


steps = [
    step(apply_step, rollback_step)
]
